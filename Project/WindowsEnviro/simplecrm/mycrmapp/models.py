#Creating my models. Using the task information, we have two models "Client" and "Project".
# 
from django.db import models
from django.contrib.auth.models import User

#Options for Project Status; It can either be active, inactive or complete.
PROJSTATUS = (
	('ACTIVE', 'ACTIVE'), 
	('INACTIVE', 'INACTIVE'),
	('COMPLETE', 'COMPLETE'),
)

#Clients should have the 3 fields defined below
#
class Client(models.Model):
	Client_Name = models.CharField(max_length=100, default = 'Client Name')
	Contact_Person = models.CharField(max_length=100, default = 'Contact Person')
	Contact_Number = models.BigIntegerField(default=0)
#
### Defining what should the system return on a query about clients using IDs and keys
	def __str__(self):
		return self.Client_Name

# Projects class has the following fields. Additionally, since a project can only be listed/connected under 1 client, 
# we have the foreign key object defining that relationship. 
#
class Project(models.Model):
	Client = models.ForeignKey(Client, on_delete=models.CASCADE)
	Project_Name = models.CharField(max_length=64, default = 'Project Name')
	Project_Status = models.CharField(max_length=255, choices=PROJSTATUS)	
#
### Defining what should the system return on a query about clients using IDs and keys
	def __str__(self):
		return self.Project_Name