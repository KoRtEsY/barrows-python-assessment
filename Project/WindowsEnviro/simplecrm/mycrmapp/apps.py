## Configuring the applications containing the different functionalities of the system.
#
from django.apps import AppConfig

class MycrmappConfig(AppConfig):
    name = 'mycrmapp'
