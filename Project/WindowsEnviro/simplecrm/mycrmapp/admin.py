# Registering our Models "Client" and "Project" have an admin interface
#
from django.contrib import admin

from .models import Client, Project

admin.site.register(Client)
admin.site.register(Project)

