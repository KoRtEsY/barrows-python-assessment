from django.db import models
from django.contrib.auth.models import User

# Create your models here.

PROJSTATUS = (
	('ACTIVE', 'ACTIVE'), 
	('INACTIVE', 'INACTIVE'),
	('COMPLETE', 'COMPLETE'),
)

class Client(models.Model):
	Client_Name = models.CharField(max_length=100, default = 'Client Name')
	Contact_Person = models.CharField(max_length=100, default = 'Contact Person')
	Contact_Number = models.BigIntegerField(default=0)

	def __str__(self):
		return self.Client_Name

class Project(models.Model):
	Project_Name = models.CharField(max_length=64, default = 'Project Name')
	Project_Status = models.CharField(max_length=255, choices=PROJSTATUS)
	Client = models.ForeignKey(Client, on_delete=models.CASCADE)

	def __str__(self):
		return self.Project_Name
	
