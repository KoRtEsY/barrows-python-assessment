#Tell the admin that the 'client list and projects' objects have an admin interface
from django.contrib import admin
from .models import Client
from .models import Project

admin.site.register(Client)
admin.site.register(Project)

